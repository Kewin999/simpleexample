package com.begoml.dataapi.dto

data class NewsDto(val newsId: Int = 0, val title: String, val message: String)