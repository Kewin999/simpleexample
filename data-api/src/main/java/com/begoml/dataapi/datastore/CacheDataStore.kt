package com.begoml.dataapi.datastore

import com.begoml.dataapi.dto.NewsDto
import io.reactivex.Single

interface CacheDataStore {

    fun getNewsModel(newsId: Int): Single<NewsDto>

    fun saveNews(news: List<NewsDto>)
}