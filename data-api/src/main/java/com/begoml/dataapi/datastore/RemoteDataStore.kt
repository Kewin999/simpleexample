package com.begoml.dataapi.datastore

import com.begoml.dataapi.dto.NewsDto
import io.reactivex.Single

interface RemoteDataStore {

    fun getNewsModels(): Single<List<NewsDto>>
}