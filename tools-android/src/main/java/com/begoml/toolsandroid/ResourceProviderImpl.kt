package com.begoml.toolsandroid

import android.content.Context
import android.content.res.Resources
import com.begoml.toolsapiandroid.ResourceProvider
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ResourceProviderImpl @Inject constructor(private val context: Context) : ResourceProvider {

    override fun getResources(): Resources {
        return context.resources
    }

    override fun getString(resId: Int): String {
        return context.getString(resId)
    }

    override fun getString(resId: Int, vararg formatArgs: Any): String {
        return context.getString(resId, *formatArgs)
    }
}