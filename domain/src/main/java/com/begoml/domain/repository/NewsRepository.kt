package com.begoml.domain.repository

import com.begoml.domain.models.NewsModel
import io.reactivex.Single

interface NewsRepository {

    fun getNewsModels(): Single<List<NewsModel>>

    fun getNewsModel(newsId: Int): Single<NewsModel>
}