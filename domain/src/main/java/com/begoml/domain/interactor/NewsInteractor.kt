package com.begoml.domain.interactor

import com.begoml.domain.models.NewsModel
import io.reactivex.Single

interface NewsInteractor {
    fun getNewsModels(): Single<List<NewsModel>>

    fun getNewsModel(newsId:Int): Single<NewsModel>
}