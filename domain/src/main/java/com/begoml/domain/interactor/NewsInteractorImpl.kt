package com.begoml.domain.interactor

import com.begoml.toolsapi.RxSchedulers
import com.begoml.domain.models.NewsModel
import com.begoml.domain.repository.NewsRepository
import io.reactivex.Single
import javax.inject.Inject

class NewsInteractorImpl @Inject constructor(
    private val rxSchedulers: com.begoml.toolsapi.RxSchedulers,
    private val repository: NewsRepository
) : NewsInteractor {


    override fun getNewsModel(newsId: Int): Single<NewsModel> {
        return repository.getNewsModel(newsId)
            .subscribeOn(rxSchedulers.io())
    }


    override fun getNewsModels(): Single<List<NewsModel>> {
        return repository.getNewsModels()
            .map {
                return@map it.filter { it.title.isNotEmpty() }
            }
            .subscribeOn(rxSchedulers.io())
    }
}