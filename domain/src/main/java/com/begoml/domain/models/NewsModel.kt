package com.begoml.domain.models

data class NewsModel(val title: String, val message: String, val newsId: Int = 0)