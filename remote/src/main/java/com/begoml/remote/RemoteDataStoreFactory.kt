package com.begoml.remote

import com.begoml.common.tools.datastore.remote.RestApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RemoteDataStoreFactory {

    private const val API_BASE_URL = "http://testurl"

    fun makeApiService(debug: Boolean): RestApi {
        val okHttpClient = makeApiService(getLoggingInterceptor(debug))

        return makeApiService(okHttpClient, getGsonBuilder(), API_BASE_URL)
    }

    private fun makeApiService(okHttpClient: OkHttpClient, gson: Gson, baseUrl: String): RestApi {
        val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        return retrofit.create(RestApi::class.java)
    }

    private fun makeApiService(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .retryOnConnectionFailure(true).build()
    }

    private fun getLoggingInterceptor(debug: Boolean): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if (debug) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
        return logging
    }


    private fun getGsonBuilder(): Gson {
        return GsonBuilder()
            .setLenient()
            .create()
    }

}