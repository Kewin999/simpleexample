package com.begoml.remote

import com.begoml.common.tools.datastore.remote.RestApi
import com.begoml.dataapi.datastore.RemoteDataStore
import com.begoml.dataapi.dto.NewsDto
import io.reactivex.Single
import javax.inject.Inject

class RemoteDataStoreImpl @Inject constructor(private val restApi: RestApi) : RemoteDataStore {

    override fun getNewsModels(): Single<List<NewsDto>> {
        return Single.fromCallable {
            val list = mutableListOf<NewsDto>()
            list.add(NewsDto(1, "Good Developers vs Bad Developers", ""))
            list.add(NewsDto(2, "Cicerone ", ""))
            list.add(NewsDto(3, "Dagger 2, Cicerone, Kotlin - inject problem", ""))
            list.add(NewsDto(4, "The Clean Architecture", ""))
            list.add(NewsDto(5, "", ""))
            return@fromCallable list
        }
    }

}