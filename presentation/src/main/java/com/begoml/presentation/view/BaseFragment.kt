package com.begoml.presentation.view

import android.content.Context
import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatFragment
import com.begoml.presentation.interfaces.ActivityListener
import com.begoml.presentation.viewmodel.MessageViewModel

abstract class BaseFragment : MvpAppCompatFragment(), BaseView {

    abstract fun inject()

    protected var activityListener: ActivityListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ActivityListener) {
            activityListener = context
        } else {
            throw RuntimeException("$context must implement INavigation")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
    }

    override fun showLoading() {
        activityListener?.showLoading()
    }

    override fun hideLoading() {
        activityListener?.hideLoading()
    }

    override fun <VM : MessageViewModel> showMessage(messageViewModel: VM) {
        activityListener?.showMessage(messageViewModel)
    }
}