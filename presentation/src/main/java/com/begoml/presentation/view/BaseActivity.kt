package com.begoml.presentation.view

import android.os.Bundle
import android.support.annotation.CallSuper
import com.arellomobile.mvp.MvpAppCompatActivity
import com.begoml.coreui.dialogs.DialogFactory
import com.begoml.coreui.widgets.ProgressDialog
import com.begoml.presentation.interfaces.ActivityListener
import com.begoml.presentation.viewmodel.MessageViewModel

abstract class BaseActivity : MvpAppCompatActivity(), BaseView, ActivityListener {

    private val progressDialog: ProgressDialog by lazy { ProgressDialog(this) }

    abstract fun inject()

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
    }

    override fun showLoading() {
        progressDialog.show()
    }

    override fun hideLoading() {
        progressDialog.hide()
    }

    override fun <VM : MessageViewModel> showMessage(messageViewModel: VM) {
        val message = messageViewModel.message
        val title = messageViewModel.title
        DialogFactory.createSimpleOkInfoDialog(this, message = message, title = title).show()
    }
}