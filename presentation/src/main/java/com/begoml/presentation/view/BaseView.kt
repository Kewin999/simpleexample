package com.begoml.presentation.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.begoml.presentation.viewmodel.MessageViewModel

@StateStrategyType(
    AddToEndSingleStrategy::class
)
interface BaseView : MvpView {

    fun showLoading()

    fun hideLoading()

    @StateStrategyType(SkipStrategy::class)
    fun <VM : MessageViewModel> showMessage(messageViewModel: VM)
}