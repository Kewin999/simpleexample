package com.begoml.presentation.tools

import io.reactivex.SingleTransformer
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer

fun <T> applySingleBeforeAndAfter(before: Consumer<Disposable>, after: Action): SingleTransformer<T, T> {
    return SingleTransformer { upstream ->
        upstream
            .doOnDispose(after)
            .doOnSubscribe(before)
            .doAfterTerminate(after)
    }
}