package com.begoml.presentation.viewmodel

data class MessageViewModel(val message: String, val title: String)