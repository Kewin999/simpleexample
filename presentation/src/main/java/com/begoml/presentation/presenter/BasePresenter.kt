package com.begoml.presentation.presenter

import com.arellomobile.mvp.MvpPresenter
import com.begoml.toolsapiandroid.ResourceProvider
import com.begoml.presentation.R
import com.begoml.presentation.tools.applySingleBeforeAndAfter
import com.begoml.presentation.view.BaseView
import com.begoml.presentation.viewmodel.MessageViewModel
import io.reactivex.SingleTransformer
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer

abstract class BasePresenter<V : BaseView> constructor(
    protected val resourceManager: com.begoml.toolsapiandroid.ResourceProvider,
    private val compositeDisposable: com.begoml.toolsapi.DisposableManager,
    protected val rxSchedulers: com.begoml.toolsapi.RxSchedulers
) :
    MvpPresenter<V>(), IBasePresenter {

    fun onThrowable(throwable: Throwable) {
        viewState.hideLoading()
        val message = throwable.message ?: ""
        val title = resourceManager.getString(R.string.title_error)
        val viewModel = MessageViewModel(message = message, title = title)
        viewState.showMessage(viewModel)
    }

    override fun detachView(view: V) {
        super.detachView(view)
        compositeDisposable.dispose()
    }

    protected fun addDisposable(@NonNull disposable: Disposable) {
        compositeDisposable.add(disposable)
    }


    protected fun showLoading(): Consumer<Disposable> {
        return Consumer { viewState.showLoading() }
    }

    protected fun hideLoading(): Action {
        return Action {
            run {
                viewState.hideLoading()
            }
        }
    }

    protected fun <T> getProgressSingleTransformer(): SingleTransformer<T, T> {
        return applySingleBeforeAndAfter(showLoading(), hideLoading())
    }

}