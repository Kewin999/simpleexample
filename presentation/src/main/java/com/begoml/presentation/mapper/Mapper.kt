package com.begoml.presentation.mapper

interface Mapper<out V, in D> {

    fun mapModelToViewModel(model: D): V
}