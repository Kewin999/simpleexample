package com.begoml.presentation.interfaces

import com.begoml.presentation.viewmodel.MessageViewModel

interface ActivityListener {

    fun showLoading()

    fun hideLoading()

    fun <VM : MessageViewModel> showMessage(messageViewModel: VM)
}