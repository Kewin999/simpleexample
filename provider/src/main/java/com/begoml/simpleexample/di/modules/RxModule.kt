package com.begoml.simpleexample.di.modules

import com.begoml.toolsapi.DisposableManager
import com.begoml.toolsapi.RxSchedulers
import com.begoml.tools.DisposableManagerImpl
import com.begoml.tools.RxSchedulersImpl
import dagger.Binds
import dagger.Module

@Module
interface RxModule {

    @Binds
    fun provideRxSchedulers(schedulers: RxSchedulersImpl): com.begoml.toolsapi.RxSchedulers

    @Binds
    fun provideDisposableManager(disposableManager: DisposableManagerImpl): com.begoml.toolsapi.DisposableManager
}