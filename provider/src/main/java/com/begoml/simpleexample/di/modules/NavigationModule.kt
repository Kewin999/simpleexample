package com.begoml.simpleexample.di.modules

import com.begoml.dataapi.datastore.RemoteDataStore
import com.begoml.navigatorglobalapi.NavigatorGlobal
import com.begoml.navigatorglobalimpl.NavigatorGlobalImpl
import com.begoml.remote.RemoteDataStoreImpl
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

@Module
class NavigationModule {

    @Provides
    @Singleton
    fun provideCicerone(): Cicerone<Router> = Cicerone.create()

    @Provides
    fun provideStartRouter(cicerone: Cicerone<Router>): Router = cicerone.router

    @Provides
    @Singleton
    fun provideStartNavigatorHolder(cicerone: Cicerone<Router>): NavigatorHolder = cicerone.navigatorHolder


    @Provides
    fun provideNavigatorGlobal(): NavigatorGlobal = NavigatorGlobalImpl()

}