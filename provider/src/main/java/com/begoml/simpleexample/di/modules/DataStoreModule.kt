package com.begoml.simpleexample.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import android.content.SharedPreferences
import com.begoml.cache.CacheDataStoreImpl
import com.begoml.cache.DbDataSource
import com.begoml.cache.PreferenceDataSourceImpl
import com.begoml.cacheapi.PreferenceDataSource
import com.begoml.common.BaseApp
import com.begoml.common.tools.datastore.remote.RestApi
import com.begoml.dataapi.datastore.CacheDataStore
import com.begoml.dataapi.datastore.RemoteDataStore
import com.begoml.remote.RemoteDataStoreFactory
import com.begoml.remote.RemoteDataStoreImpl
import com.begoml.simpleexample.BuildConfig
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class DataStoreModule {

    @Module
    companion object {

        private const val PREF_PACKAGE_NAME = "com.begoml.simpleexample.cache.preferences"

        @Singleton
        @Provides
        @JvmStatic
        fun provideSharedPreferences(app: BaseApp): SharedPreferences {
            return app.getApplicationContext().getSharedPreferences(PREF_PACKAGE_NAME, Context.MODE_PRIVATE)
        }

        @Singleton
        @Provides
        @JvmStatic
        fun provideRestApi(): RestApi {
            return RemoteDataStoreFactory.makeApiService(BuildConfig.DEBUG)
        }

        @Singleton
        @Provides
        @JvmStatic
        fun provideAppDatabase(app: BaseApp): DbDataSource {
            return Room.databaseBuilder(app.getApplicationContext(), DbDataSource::class.java, "test-database").build()
        }

        @Singleton
        @Provides
        @JvmStatic
        fun providePreferenceManager(sharedPreferences: SharedPreferences): PreferenceDataSource {
            return PreferenceDataSourceImpl(sharedPreferences)
        }
    }

    @Binds
    abstract fun provideRemoteDataStore(remote: RemoteDataStoreImpl): RemoteDataStore

    @Binds
    abstract fun provideCacheDataStore(dataStore: CacheDataStoreImpl): CacheDataStore

}