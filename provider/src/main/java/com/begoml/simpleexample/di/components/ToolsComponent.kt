package com.begoml.simpleexample.di.components

import com.begoml.common.BaseApp
import com.begoml.common.di.ToolsProvider
import com.begoml.simpleexample.di.modules.*
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, ToolsModule::class, RxModule::class, NavigationModule::class, DataStoreModule::class])
interface ToolsComponent : ToolsProvider {

    @Component.Builder
    interface Builder {

        fun build(): ToolsComponent

        @BindsInstance
        fun app(app: BaseApp): Builder
    }

    class Initializer private constructor() {
        companion object {

            fun init(app: BaseApp): ToolsComponent = DaggerToolsComponent.builder()
                .app(app)
                .build()
        }
    }
}