package com.begoml.simpleexample.di.modules

import com.begoml.toolsapiandroid.ResourceProvider
import com.begoml.toolsandroid.ResourceProviderImpl
import dagger.Binds
import dagger.Module

@Module
interface ToolsModule {

    @Binds
    fun provideResourceProvider(res: ResourceProviderImpl): com.begoml.toolsapiandroid.ResourceProvider
}