package com.begoml.simpleexample.di.components

import com.begoml.common.BaseApp
import com.begoml.common.di.AppProvider
import com.begoml.common.di.ToolsProvider
import com.begoml.simpleexample.BaseAppImpl
import dagger.Component
import javax.inject.Singleton

@Component(dependencies = [ToolsProvider::class])
@Singleton
interface AppComponent : AppProvider {

    fun inject(app: BaseAppImpl)

    class Initializer private constructor() {
        companion object {

            fun init(app: BaseApp): AppComponent {
                val mainToolsProvider = ToolsComponent.Initializer.init(app)
                return DaggerAppComponent.builder()
                    .toolsProvider(mainToolsProvider)
                    .build()
            }
        }
    }
}