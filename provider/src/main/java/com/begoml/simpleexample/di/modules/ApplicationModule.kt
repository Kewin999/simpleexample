package com.begoml.simpleexample.di.modules

import com.begoml.common.BaseApp
import dagger.Module
import dagger.Provides

@Module
abstract class ApplicationModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideContext(app: BaseApp) = app.getApplicationContext()
    }
}