package com.begoml.simpleexample

import android.app.Application
import com.arellomobile.mvp.RegisterMoxyReflectorPackages
import com.begoml.common.BaseApp
import com.begoml.common.di.AppProvider
import com.begoml.navigatorglobalapi.NavigatorGlobal
import com.begoml.simpleexample.di.components.AppComponent
import javax.inject.Inject

@RegisterMoxyReflectorPackages(
    "com.begoml.featurestart"
)
class BaseAppImpl : Application(), BaseApp {

    private val appComponent: AppComponent by lazy { AppComponent.Initializer.init(this@BaseAppImpl) }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
    }

    override fun getAppComponent(): AppProvider = appComponent
}