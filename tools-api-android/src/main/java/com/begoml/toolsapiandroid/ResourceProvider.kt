package com.begoml.toolsapiandroid

import android.content.res.Resources
import android.support.annotation.*

interface ResourceProvider {

    fun getResources(): Resources

    fun getString(@StringRes resId: Int): String

    fun getString(@StringRes resId: Int, vararg formatArgs: Any): String

}