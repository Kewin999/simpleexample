package com.begoml.data

import com.begoml.dataapi.datastore.CacheDataStore
import com.begoml.dataapi.datastore.RemoteDataStore
import com.begoml.domain.models.NewsModel
import com.begoml.domain.repository.NewsRepository
import io.reactivex.Single
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(
    private val remoteDataStore: RemoteDataStore,
    private val cacheDataStore: CacheDataStore
) : NewsRepository {

    override fun getNewsModel(newsId: Int): Single<NewsModel> {
        return cacheDataStore.getNewsModel(newsId)
            .map { NewsModel(it.title, it.message) }
    }


    override fun getNewsModels(): Single<List<NewsModel>> {
        return remoteDataStore.getNewsModels()
            .map {
                cacheDataStore.saveNews(it)
                it
            }
            .map { news ->
                news.map { NewsModel(it.title, it.message, newsId = it.newsId) }
            }
    }
}