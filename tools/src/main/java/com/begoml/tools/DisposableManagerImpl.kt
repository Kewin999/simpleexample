package com.begoml.tools

import com.begoml.toolsapi.DisposableManager
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class DisposableManagerImpl @Inject constructor() : DisposableManager {

    private var compositeDisposable: CompositeDisposable? = null

    override fun add(disposable: Disposable) {
        getCompositeDisposable().add(disposable)
    }

    override fun dispose() {
        getCompositeDisposable().dispose()
    }

    private fun getCompositeDisposable(): CompositeDisposable {
        if (compositeDisposable == null || compositeDisposable!!.isDisposed) {
            compositeDisposable = CompositeDisposable()
        }
        return compositeDisposable as CompositeDisposable
    }}