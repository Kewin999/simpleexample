package com.begoml.navigatorglobalapi

/**
 * Use it for navigation between features
 */
interface NavigatorGlobal {
    fun startNewFeature()
}