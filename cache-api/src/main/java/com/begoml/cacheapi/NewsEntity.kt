package com.begoml.cacheapi

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "news")
data class NewsEntity(@PrimaryKey var id: Int, val title: String, val message: String)