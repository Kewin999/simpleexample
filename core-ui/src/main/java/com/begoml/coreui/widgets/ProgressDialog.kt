package com.begoml.coreui.widgets

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatDialog
import com.begoml.coreui.R

class ProgressDialog(context: Context) {

    private var progressDialog: AppCompatDialog = AppCompatDialog(context)

    init {
        progressDialog.setContentView(R.layout.progress_diallog)
        progressDialog.setCancelable(false)
        progressDialog.window?.setBackgroundDrawable(
            ColorDrawable(Color.TRANSPARENT)
        )
    }

    fun show() {
        if (!progressDialog.isShowing) {
            progressDialog.show()
        }
    }

    fun hide() {
        if (progressDialog.isShowing) {
            progressDialog.dismiss()
        }
    }
}