package com.begoml.coreui.dialogs

import android.app.Dialog
import android.content.Context
import android.support.v7.app.AlertDialog
import com.begoml.coreui.R

object DialogFactory {

    fun createSimpleOkInfoDialog(context: Context, title: String = "", message: String): Dialog {
        val builder = AlertDialog.Builder(context)
        with(builder)
        {
            setTitle(title)
            setMessage(message)
            setPositiveButton(R.string.title_ok) { dialogInterface, _ -> dialogInterface.cancel() }
        }

        return builder.create()
    }
}