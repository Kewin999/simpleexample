package com.begoml.coreui.adapters.viewholders

import android.support.v7.widget.RecyclerView
import android.view.View
import com.begoml.coreui.adapters.BaseAdapter

abstract class BaseViewHolder<I, VM>(itemView: View) : RecyclerView.ViewHolder(itemView), BaseAdapter.IBinder<VM> {

    private var listener: I? = null


    abstract override fun bindViewModel(vm: VM)

    fun getListener(): I? = listener

    fun <C> setListener(callbacks: C) {
        this.listener = callbacks as I
    }
}