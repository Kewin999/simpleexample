package com.begoml.coreui.adapters

import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.begoml.coreui.adapters.viewholders.BaseViewHolder

abstract class BaseAdapter<VM, C, VH :
BaseViewHolder<*, VM>>(@NonNull var viewModels: List<VM>, private val listener: C) :
    RecyclerView.Adapter<VH>() {

    internal interface IBinder<in T> {
        fun bindViewModel(vm: T)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return getBindingViewHolder(viewType, parent)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.setListener(listener)

        val viewModel = viewModels[position]
        (holder as IBinder<VM>).bindViewModel(viewModel)
    }

    protected abstract fun getBindingViewHolder(viewType: Int, parent: ViewGroup): VH

    override fun getItemCount() = viewModels.size
}