package com.begoml.featurestart.presenter.details

import com.arellomobile.mvp.InjectViewState
import com.begoml.domain.interactor.NewsInteractor
import com.begoml.featurestart.mapper.NewsDetailsViewModelMapper
import com.begoml.featurestart.view.details.NewsDetailsVeiw
import com.begoml.presentation.presenter.BasePresenter
import com.begoml.toolsapi.DisposableManager
import com.begoml.toolsapi.RxSchedulers
import com.begoml.toolsapiandroid.ResourceProvider
import javax.inject.Inject

@InjectViewState
class NewsDetailsPresenter @Inject constructor(
    private val newsId: Int,
    private val newsInteractor: NewsInteractor,
    private val mapper: NewsDetailsViewModelMapper,
    rxSchedulers: RxSchedulers,
    resourceManager: ResourceProvider,
    disposableManager: DisposableManager
) : BasePresenter<NewsDetailsVeiw>(
    resourceManager,
    disposableManager,
    rxSchedulers
), INewsDetailsPresenter {


    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        addDisposable(
            newsInteractor.getNewsModel(newsId)
                .observeOn(rxSchedulers.androidThread())
                .compose(getProgressSingleTransformer())
                .subscribe({
                    val vm = mapper.mapModelToViewModel(it)
                    viewState.setViewModel(vm)
                }, { throwable -> onThrowable(throwable) })
        )
    }
}