package com.begoml.featurestart.presenter.list

import com.arellomobile.mvp.InjectViewState
import com.begoml.toolsapiandroid.ResourceProvider
import com.begoml.domain.interactor.NewsInteractor
import com.begoml.domain.models.NewsModel
import com.begoml.featurestart.R
import com.begoml.featurestart.mapper.NewsViewModelMapper
import com.begoml.featurestart.view.details.NewsDetailsFragment
import com.begoml.featurestart.view.news.NewsItemListener
import com.begoml.featurestart.view.news.NewsVeiw
import com.begoml.featurestart.viewmodel.NewsViewModel
import com.begoml.presentation.presenter.BasePresenter
import com.begoml.presentation.viewmodel.MessageViewModel
import com.begoml.toolsapi.RxSchedulers
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppScreen
import javax.inject.Inject

@InjectViewState
class NewsPresenter @Inject constructor(
    private val router: Router,
    private val newsInteractor: NewsInteractor,
    private val mapper: NewsViewModelMapper,
    rxSchedulers: RxSchedulers,
    resourceManager: ResourceProvider,
    disposableManager: com.begoml.toolsapi.DisposableManager
) : BasePresenter<NewsVeiw>(
    resourceManager,
    disposableManager,
    rxSchedulers
), INewsPresenter, NewsItemListener {

    private lateinit var viewModels: List<NewsViewModel>

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        addDisposable(
            newsInteractor.getNewsModels()
                .observeOn(rxSchedulers.androidThread())
                .compose(getProgressSingleTransformer())
                .subscribe({ getNewsModelsSuccessful(it) }, { throwable -> onThrowable(throwable) })
        )
    }

    private fun getNewsModelsSuccessful(list: List<NewsModel>) {
        viewModels = list.map { mapper.mapModelToViewModel(it) }
        viewState.setViewModels(viewModels)
    }

    override fun onViewModelClick(viewModel: NewsViewModel) {
        val newsId = viewModel.newsId
        if (newsId == 2) {
            val title = resourceManager.getString(R.string.news_can_not_show_title)
            val message = resourceManager.getString(R.string.news_can_not_show_message)
            val model = MessageViewModel(title = title, message = message)
            viewState.showMessage(model)
        } else {
            router.navigateTo(NewsDetailsScreen(viewModel.newsId))
        }
    }
}

data class NewsDetailsScreen(val itemId: Int) : SupportAppScreen() {
    override fun getFragment() = NewsDetailsFragment.newInstance(itemId)
}