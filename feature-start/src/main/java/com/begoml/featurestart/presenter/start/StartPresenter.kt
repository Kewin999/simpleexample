package com.begoml.featurestart.presenter.start

import com.arellomobile.mvp.InjectViewState
import com.begoml.toolsapiandroid.ResourceProvider
import com.begoml.featurestart.view.news.NewsFragment
import com.begoml.featurestart.view.start.StartView
import com.begoml.presentation.presenter.BasePresenter
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppScreen
import javax.inject.Inject

@InjectViewState
open class StartPresenter @Inject constructor(
    private val router: Router,
    rxSchedulers: com.begoml.toolsapi.RxSchedulers,
    resourceManager: com.begoml.toolsapiandroid.ResourceProvider,
    disposableManager: com.begoml.toolsapi.DisposableManager
) : BasePresenter<StartView>(
    resourceManager,
    disposableManager,
    rxSchedulers
), IStartPresenter {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        router.newRootScreen(NewsScreen)
    }
}

object NewsScreen : SupportAppScreen() {
    override fun getFragment() = NewsFragment()
}