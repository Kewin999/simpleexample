package com.begoml.featurestart.mapper

import com.begoml.domain.models.NewsModel
import com.begoml.featurestart.viewmodel.NewsDetailsViewModel
import com.begoml.presentation.mapper.Mapper
import javax.inject.Inject

class NewsDetailsViewModelMapper @Inject constructor() : Mapper<NewsDetailsViewModel, NewsModel> {
    override fun mapModelToViewModel(model: NewsModel): NewsDetailsViewModel {
        return NewsDetailsViewModel(title = model.title, message = model.message)
    }


}