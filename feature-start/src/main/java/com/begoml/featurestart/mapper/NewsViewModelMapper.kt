package com.begoml.featurestart.mapper

import com.begoml.domain.models.NewsModel
import com.begoml.featurestart.viewmodel.NewsViewModel
import com.begoml.presentation.mapper.Mapper
import javax.inject.Inject

class NewsViewModelMapper @Inject constructor() : Mapper<NewsViewModel, NewsModel> {
    override fun mapModelToViewModel(model: NewsModel): NewsViewModel {
        return NewsViewModel(title = model.title, newsId = model.newsId)
    }


}