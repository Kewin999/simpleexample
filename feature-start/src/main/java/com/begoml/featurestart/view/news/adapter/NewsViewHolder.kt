package com.begoml.featurestart.view.news.adapter

import android.view.View
import com.begoml.coreui.adapters.viewholders.BaseViewHolder
import com.begoml.featurestart.view.news.NewsItemListener
import com.begoml.featurestart.viewmodel.NewsViewModel
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_news.*

class NewsViewHolder(override val containerView: View) :
    BaseViewHolder<NewsItemListener, NewsViewModel>(containerView),
    LayoutContainer {

    override fun bindViewModel(vm: NewsViewModel) {
        tvTitle.text = vm.title
        flContainer.setOnClickListener { getListener()?.onViewModelClick(vm) }
    }
}