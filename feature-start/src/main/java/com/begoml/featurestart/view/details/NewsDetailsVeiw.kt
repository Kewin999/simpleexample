package com.begoml.featurestart.view.details

import com.begoml.featurestart.viewmodel.NewsDetailsViewModel
import com.begoml.presentation.view.BaseView

interface NewsDetailsVeiw : BaseView {

    fun setViewModel(vm: NewsDetailsViewModel)
}