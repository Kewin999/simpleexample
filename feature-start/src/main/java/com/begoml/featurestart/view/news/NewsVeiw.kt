package com.begoml.featurestart.view.news

import com.begoml.featurestart.viewmodel.NewsViewModel
import com.begoml.presentation.view.BaseView

interface NewsVeiw : BaseView {
    fun setViewModels(viewModels: List<NewsViewModel>)
}