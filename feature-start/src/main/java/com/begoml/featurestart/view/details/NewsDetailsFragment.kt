package com.begoml.featurestart.view.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.begoml.common.BaseApp
import com.begoml.featurestart.R
import com.begoml.featurestart.di.details.NewsDetailsComponent
import com.begoml.featurestart.presenter.details.NewsDetailsPresenter
import com.begoml.featurestart.viewmodel.NewsDetailsViewModel
import com.begoml.navigatorglobalapi.NavigatorGlobal
import com.begoml.presentation.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_news_details.*
import javax.inject.Inject

class NewsDetailsFragment : BaseFragment(), NewsDetailsVeiw {

    @Inject
    @InjectPresenter
    lateinit var presenter: NewsDetailsPresenter

    @Inject
    lateinit var navigatorGlobal: NavigatorGlobal

    @ProvidePresenter
    fun providePresenter() = presenter

    companion object {

        private const val ARGS_ITEM_ID = "args.item.id"

        fun newInstance(itemId: Int): NewsDetailsFragment {
            val fragment = NewsDetailsFragment()
            val bundle = Bundle()
            bundle.putInt(ARGS_ITEM_ID, itemId)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_news_details, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnGoToOtherFeature.setOnClickListener {
            navigatorGlobal.startNewFeature()
        }
    }

    override fun inject() {
        val newsId = arguments?.getInt(ARGS_ITEM_ID) ?: 0
        NewsDetailsComponent.Initializer
            .init((context?.applicationContext as BaseApp).getAppComponent(), newsId)
            .inject(this@NewsDetailsFragment)
    }

    override fun setViewModel(vm: NewsDetailsViewModel) {
        tvTitle.text = vm.title
        tvMessage.text = vm.message
    }
}