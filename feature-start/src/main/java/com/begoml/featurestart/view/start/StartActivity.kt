package com.begoml.featurestart.view.start

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.begoml.common.BaseApp
import com.begoml.featurestart.R
import com.begoml.featurestart.di.start.MainComponent
import com.begoml.featurestart.presenter.start.StartPresenter
import com.begoml.presentation.view.BaseActivity
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject


class StartActivity : BaseActivity(), StartView {

    @Inject
    @InjectPresenter
    lateinit var presenter: StartPresenter

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var navigatorRouterHolder: NavigatorHolder

    private lateinit var navigatorHolder: Navigator

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        navigatorHolder = SupportAppNavigator(this, R.id.flMainContainer)
    }

    override fun onResume() {
        super.onResume()
        navigatorRouterHolder.setNavigator(navigatorHolder)
    }

    override fun onPause() {
        navigatorRouterHolder.removeNavigator()
        super.onPause()
    }

    override fun inject() {
        MainComponent.Initializer
            .init((applicationContext as BaseApp).getAppComponent())
            .inject(this@StartActivity)
    }
}