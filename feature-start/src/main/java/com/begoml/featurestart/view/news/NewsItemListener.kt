package com.begoml.featurestart.view.news

import com.begoml.featurestart.viewmodel.NewsViewModel

interface NewsItemListener {

    fun onViewModelClick(viewModel: NewsViewModel)
}