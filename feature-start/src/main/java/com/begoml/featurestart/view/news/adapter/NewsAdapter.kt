package com.begoml.featurestart.view.news.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.begoml.coreui.adapters.BaseAdapter
import com.begoml.featurestart.R
import com.begoml.featurestart.view.news.NewsItemListener
import com.begoml.featurestart.viewmodel.NewsViewModel

class NewsAdapter(viewModels: List<NewsViewModel>, listener: NewsItemListener) :
    BaseAdapter<NewsViewModel, NewsItemListener, NewsViewHolder>(viewModels, listener) {

    override fun getBindingViewHolder(viewType: Int, parent: ViewGroup): NewsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_news, parent, false)
        return NewsViewHolder(view)
    }
}