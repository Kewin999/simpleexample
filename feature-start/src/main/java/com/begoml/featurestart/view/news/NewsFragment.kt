package com.begoml.featurestart.view.news

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.begoml.common.BaseApp
import com.begoml.featurestart.R
import com.begoml.featurestart.di.news.NewsComponent
import com.begoml.featurestart.presenter.list.NewsPresenter
import com.begoml.featurestart.view.news.adapter.NewsAdapter
import com.begoml.featurestart.viewmodel.NewsViewModel
import com.begoml.navigatorglobalapi.NavigatorGlobal
import com.begoml.presentation.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.android.synthetic.main.view_toolbar.*
import javax.inject.Inject

class NewsFragment : BaseFragment(), NewsVeiw {

    @Inject
    @InjectPresenter
    lateinit var presenter: NewsPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    @Inject
    lateinit var navigatorGlobal: NavigatorGlobal

    private lateinit var adapter: NewsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_news, null)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        rvNewsList.layoutManager = LinearLayoutManager(context)
        toolbar.title = getString(R.string.news_title)
    }

    override fun setViewModels(viewModels: List<NewsViewModel>) {
        adapter = NewsAdapter(viewModels, presenter)
        rvNewsList.adapter = adapter
    }

    override fun inject() {
        NewsComponent.Initializer
            .init((context?.applicationContext as BaseApp).getAppComponent())
            .inject(this@NewsFragment)
    }
}