package com.begoml.featurestart.viewmodel

data class NewsViewModel(val title: String, val newsId: Int)