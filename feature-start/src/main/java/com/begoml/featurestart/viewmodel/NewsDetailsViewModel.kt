package com.begoml.featurestart.viewmodel

data class NewsDetailsViewModel(val title: String, val message: String)