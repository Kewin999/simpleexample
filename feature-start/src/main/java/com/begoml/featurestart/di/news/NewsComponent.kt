package com.begoml.featurestart.di.news

import com.begoml.common.di.AppProvider
import com.begoml.common.di.FragmentScope
import com.begoml.featurestart.di.core.CoreModule
import com.begoml.featurestart.view.news.NewsFragment
import dagger.Component

@FragmentScope
@Component(
    dependencies = [AppProvider::class],
    modules = [NewsModule::class, CoreModule::class]
)
interface NewsComponent {

    fun inject(fragment: NewsFragment)

    class Initializer private constructor() {
        companion object {
            fun init(appProvider: AppProvider): NewsComponent = DaggerNewsComponent
                .builder()
                .appProvider(appProvider)
                .build()
        }
    }
}