package com.begoml.featurestart.di.details

import com.begoml.domain.interactor.NewsInteractor
import com.begoml.featurestart.mapper.NewsDetailsViewModelMapper
import com.begoml.featurestart.presenter.details.INewsDetailsPresenter
import com.begoml.featurestart.presenter.details.NewsDetailsPresenter
import com.begoml.toolsapi.DisposableManager
import com.begoml.toolsapi.RxSchedulers
import com.begoml.toolsapiandroid.ResourceProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NewsDetailsModule(val newsId: Int) {

    @Provides
    fun provideNewsId(): Int {
        return newsId
    }

    @Module
    companion object {

        @Singleton
        @Provides
        @JvmStatic
        fun provideNewsDetailsPresenter(
            newsId: Int,
            newsInteractor: NewsInteractor,
            mapper: NewsDetailsViewModelMapper,
            rxSchedulers: RxSchedulers,
            resourceManager: ResourceProvider,
            disposableManager: DisposableManager
        ): INewsDetailsPresenter {
            return NewsDetailsPresenter(
                newsId,
                newsInteractor, mapper,
                rxSchedulers,
                resourceManager,
                disposableManager
            )
        }
    }
}