package com.begoml.featurestart.di.news

import com.begoml.common.di.FragmentScope
import com.begoml.data.NewsRepositoryImpl
import com.begoml.domain.interactor.NewsInteractor
import com.begoml.domain.interactor.NewsInteractorImpl
import com.begoml.domain.repository.NewsRepository
import com.begoml.featurestart.presenter.list.INewsPresenter
import com.begoml.featurestart.presenter.list.NewsPresenter
import dagger.Binds
import dagger.Module

@Module
interface NewsModule {

    @FragmentScope
    @Binds
    fun provideNewsPresenter(presenter: NewsPresenter): INewsPresenter
}