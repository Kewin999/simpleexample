package com.begoml.featurestart.di.core

import com.begoml.data.NewsRepositoryImpl
import com.begoml.domain.interactor.NewsInteractor
import com.begoml.domain.interactor.NewsInteractorImpl
import com.begoml.domain.repository.NewsRepository
import dagger.Binds
import dagger.Module

@Module
interface CoreModule {

    @Binds
    fun provideNewsInteractor(interactor: NewsInteractorImpl): NewsInteractor

    @Binds
    fun provideNewsRepository(repo: NewsRepositoryImpl): NewsRepository
}