package com.begoml.featurestart.di.start

import com.begoml.common.di.ActivityScope
import com.begoml.common.di.FragmentScope
import com.begoml.data.NewsRepositoryImpl
import com.begoml.domain.interactor.NewsInteractor
import com.begoml.domain.interactor.NewsInteractorImpl
import com.begoml.domain.repository.NewsRepository
import com.begoml.featurestart.presenter.start.IStartPresenter
import com.begoml.featurestart.presenter.start.StartPresenter
import dagger.Binds
import dagger.Module

@Module
interface MainModule {

    @ActivityScope
    @Binds
    fun provideStartPresenter(presenter: StartPresenter): IStartPresenter
}