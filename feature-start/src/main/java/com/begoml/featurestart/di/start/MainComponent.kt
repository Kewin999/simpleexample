package com.begoml.featurestart.di.start

import com.begoml.common.di.ActivityScope
import com.begoml.common.di.AppProvider
import com.begoml.featurestart.view.start.StartActivity
import dagger.Component

@ActivityScope
@Component(
    dependencies = [AppProvider::class],
    modules = [MainModule::class]
)
interface MainComponent {

    fun inject(activity: StartActivity)

    class Initializer private constructor() {
        companion object {
            fun init(appProvider: AppProvider): MainComponent = DaggerMainComponent.builder()
                .appProvider(appProvider)
                .build()

        }
    }
}