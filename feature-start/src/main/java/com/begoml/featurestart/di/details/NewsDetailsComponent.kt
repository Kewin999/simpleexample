package com.begoml.featurestart.di.details

import com.begoml.common.di.AppProvider
import com.begoml.common.di.FragmentScope
import com.begoml.featurestart.di.core.CoreModule
import com.begoml.featurestart.view.details.NewsDetailsFragment
import dagger.Component

@FragmentScope
@Component(
    dependencies = [AppProvider::class],
    modules = [NewsDetailsModule::class, CoreModule::class]
)
interface NewsDetailsComponent {

    fun inject(fragment: NewsDetailsFragment)

    class Initializer private constructor() {
        companion object {
            fun init(appProvider: AppProvider, newsId: Int): NewsDetailsComponent = DaggerNewsDetailsComponent
                .builder()
                .newsDetailsModule(NewsDetailsModule(newsId))
                .appProvider(appProvider)
                .build()
        }
    }
}