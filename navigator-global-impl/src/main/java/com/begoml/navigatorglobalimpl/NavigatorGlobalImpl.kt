package com.begoml.navigatorglobalimpl

import com.begoml.navigatorglobalapi.NavigatorGlobal
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NavigatorGlobalImpl @Inject constructor() : NavigatorGlobal {

    override fun startNewFeature() {
        //todo will be start other feature
    }
}