package com.begoml.toolsapi

import io.reactivex.disposables.Disposable

interface DisposableManager {

    fun add(disposable: Disposable)

    fun dispose()
}