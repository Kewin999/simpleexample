package com.begoml.common.annotations

import javax.inject.Qualifier

@Qualifier
@Retention
annotation class RouterStart