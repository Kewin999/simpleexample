package com.begoml.common.di

import com.begoml.common.BaseApp
import com.begoml.toolsapiandroid.ResourceProvider
import com.begoml.dataapi.datastore.CacheDataStore
import com.begoml.dataapi.datastore.RemoteDataStore
import com.begoml.navigatorglobalapi.NavigatorGlobal
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

interface AppProvider : ToolsProvider

interface ToolsProvider {

    fun provideApp(): BaseApp

    fun provideRxSchedulers(): com.begoml.toolsapi.RxSchedulers

    fun provideDisposableManager(): com.begoml.toolsapi.DisposableManager

    fun provideResourceProvider(): com.begoml.toolsapiandroid.ResourceProvider

    fun provideNavigatorGlobal(): NavigatorGlobal

    fun provideRouterStart(): Router

    fun provideStartNavigatorHolder(): NavigatorHolder

    fun provideRemoteDataStore(): RemoteDataStore

    fun provideCacheDataStore(): CacheDataStore
}