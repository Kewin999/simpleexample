package com.begoml.common.di

@javax.inject.Scope annotation class ActivityScope
@javax.inject.Scope annotation class FragmentScope
@javax.inject.Scope annotation class ViewScope
@javax.inject.Scope annotation class ServiceScope
@javax.inject.Scope annotation class PerFlow