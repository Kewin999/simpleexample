package com.begoml.common

import android.content.Context
import com.begoml.common.di.AppProvider

interface BaseApp {

    fun getApplicationContext(): Context

    fun getAppComponent(): AppProvider
}