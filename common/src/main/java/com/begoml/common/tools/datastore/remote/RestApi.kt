package com.begoml.common.tools.datastore.remote

import com.begoml.dataapi.dto.NewsDto
import io.reactivex.Single
import retrofit2.http.GET

interface RestApi {

    @GET("test")
    fun getNews(): Single<NewsDto>
}