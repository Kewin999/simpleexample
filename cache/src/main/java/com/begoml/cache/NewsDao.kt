package com.begoml.cache

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.begoml.cacheapi.NewsEntity

@Dao
interface NewsDao {

    @Insert
    fun insert(entity: NewsEntity)

    @Delete
    fun delete(entity: NewsEntity)

    @Query("DELETE  FROM news")
    fun deleteAllNews()

    @Query("SELECT * FROM news")
    fun getAllNews(): List<NewsEntity>
}