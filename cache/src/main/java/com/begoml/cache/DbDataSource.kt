package com.begoml.cache

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.begoml.cacheapi.NewsEntity

@Database(entities = [NewsEntity::class], version = 1, exportSchema = false)
abstract class DbDataSource : RoomDatabase() {

    abstract fun getNewsDao(): NewsDao
}