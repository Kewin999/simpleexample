package com.begoml.cache

import android.content.SharedPreferences
import com.begoml.cacheapi.PreferenceDataSource
import javax.inject.Inject

class PreferenceDataSourceImpl @Inject constructor(private val preferences: SharedPreferences) :
    PreferenceDataSource {
}