package com.begoml.cache

import com.begoml.cacheapi.NewsEntity
import com.begoml.cacheapi.PreferenceDataSource
import com.begoml.dataapi.datastore.CacheDataStore
import com.begoml.dataapi.dto.NewsDto
import io.reactivex.Single
import javax.inject.Inject

/**
 * use this for example: database, preference and other cache
 */
class CacheDataStoreImpl @Inject constructor(
    private val preferenceDataSource: PreferenceDataSource,
    private val db: DbDataSource
) : CacheDataStore {


    override fun saveNews(news: List<NewsDto>) {
        val dao = db.getNewsDao()
        dao.deleteAllNews()
        news.map { NewsEntity(id = it.newsId, title = it.title, message = it.message) }
            .forEach {
                dao.insert(it)
            }
    }


    override fun getNewsModel(newsId: Int): Single<NewsDto> {
        return Single.fromCallable {
            return@fromCallable NewsDto(
                title = "Good Developers vs Bad Developers",
                message = "There’s a saying that “a great programmer can be 10 times as good as a mediocre one”.\n" +
                        "No one really wants to be labeled as a bad developer, but the sad reality is that a lot of developers aren’t even aware that they’re bad. So what makes a developer bad, and what makes them good? This article will go through the characteristics of bad developers, good developers, and really good developers.\n" +
                        "(If you want to have fun and take a quiz to find out whether you’re a good developer, feel free to check out the original article!) \n\n" +
                        "If you’re still a beginner to coding and you’re worried if you’re producing bad code, in terms of ability you are naturally not good right now. However, don’t feel discouraged as there is one major characteristic that makes a developer bad, and as long as you don’t fall into that trap, you have room for improvement (i.e. You’re green, not bad).\n" +
                        "That said, let’s first understand what are the two main types of bad developers:\n" +
                        "The Cowboy/girl Coder (for the sake of reading ease let’s just use “cowboy” when referring to this type)\n" +
                        "The Mediocre Dev\n" +
                        "At the core they are the same, but they usually exhibit different behaviors. \n\n" +
                        "Cowboy Coders \n" +
                        "Cowboy coders would destroy a team and they work best by themselves and on a single project with a short life-span.\n" +
                        "Self-taught coders who never received any guidance with how to write usable code are usually in danger of becoming one of these, and many good, experienced programmers have likely been a cowboy at the beginning of their coding careers. So what are key attributes of a cowboy coder?\n" +
                        "Codes Very Quickly\n" +
                        "Usually these types of bad devs can churn out new features far more quickly than the average dev, and unfortunately people who don’t know code would think these speedy coders are awesome (which only further bolsters the cowboy’s ego). These devs work best alone and for clients with extremely tight deadlines and who are only looking to get feature out as soon as possible.\n" +
                        "Cowboy coders code very fast because they usually code on the fly — meaning, they code without any planning for future maintainability. Which leads to…\n\n" +
                        "Messy, Unreadable Code\n" +
                        "The code design of quickly built projects would be a complete mess (or rather, code design is non-existent). This sort of messy code is often referred to as “Spaghetti Code”, which is not at all as tasty as it sounds.\n" +
                        "\n" +
                        "it’s called Spaghetti because everything is jumbled together and impossible to separate\n" +
                        "Spaghetti code is difficult to understand and is usually unnecessarily large and complex to the point where others will find it difficult to understand what the programmer does, and thus it is usually a nightmare to maintain. This means decreased productivity for the whole team if anyone is unfortunate enough to have to work with a cowboy coder.\n" +
                        "The result of messy code is…\n" +
                        "3. Bugs. Bugs, Everywhere\n" +
                        "\n" +
                        "If a company’s software grows larger and more complex and their code is still a pile of spaghetti, then it’s just a ticking bomb waiting to explode. At the worst it would cause problems as severe as Toyota’s unintended acceleration. Everyone can agree that the Toyota car recall was a disaster.\n" +
                        "It’s also never pleasant if your software happens to enter the hall of shame at the Daily WTF.\n" +
                        "\n" +
                        "As the Joke goes: “99 little bugs in the code 99 little bugs in the code Take one down, patch it around 117 little bugs in the code” (source)\n" +
                        "What’s more, spaghetti code is not extensible. This means that adding new features to a Spaghetti code is like walking in a minefield that will explode, no matter how large or small the step and what direction you take. This is usually because a cowboy coder jumbled every functionality together, so any change would break the software altogether. This could be prevented with better code design and/or unit tests, but of course, cowboys don’t give a care for whether their code is usable and also don’t care for writing tests (something that takes time). Not to mention, with the way their code is structured thanks to bad design decisions, it’s hardly going to be testable or even debuggable anyway. What usually happens with a cowboy coder is they quickly “fix” some bug, only to create more bugs. They’d likely feel like busy, heroic firefighters who never actually put out the source of the fire.\n" +
                        "All in all, every bug and error created by a bad developer would cause negative productivity. At first it seems that this cowboy is being super productive by always meeting deadlines other developers won’t dare to promise, but this is at the cost of loads of “unexpected” errors that could have been prevented by well-designed and clean code programmed by a good dev.\n" +
                        "If you’re spending more than 80% of your development time debugging your own code and if your code is a nightmare to debug (i.e. you end up creating another bug), this usually means the codebase is not good and you may need help with improving your code.\n" +
                        "Arrogance\n" +
                        "Sometimes cowboy programmers are not bad because they want to be, and they’re simply cooking up spaghetti code because management/clients had impossible deadlines (though as the saying goes, any developer who takes pride in their code would GTFO those companies or decline such clients). Many beginners and junior devs go through the mistake of coding without planning and producing a bunch of buggy code, sometimes because they have less experience with these problems so they make bad decisions.\n" +
                        "These beginners can be easily straightened up by receiving mentorship from experienced devs who take pride in building quality code, as a lot of times beginners are cowboys because they didn’t know better. However, if they’re surrounded by equally bad or mediocre developers, then they would be in trouble of falling into the delusion that they’re good.\n" +
                        "As long as you are willing to take responsibility for your mistakes and as long as you are learning from your mistakes, you’re not a bad developerThe most important attribute that makes these programmers bad is arrogance.\n" +
                        "\n" +
                        "the typical attitude of an arrogant programmer\n" +
                        "Bad programmers think their code is perfect and would blame customers for being stupid and for crashing their program rather than reflect on why their software crashed. Cowboy coders are usually selfish devs who don’t have a shred of empathy for others who have to clean up after all the problems they’ve created.\n" +
                        "What’s more, these arrogant programmers also think others are beneath them in terms of intelligence. They’d usually assume people who need comments and who don’t understand their code are too dumb to work with them, but never try to think about why people don’t understand their code. As a result of always thinking they’re right and always thinking other people are inferior, they are uncommunicative when they build features, which can cause a lot of problems for a team. Some may think they’re so good that they would sometimes shun “best practices” or “standards” as they assume their own code is better (without good reason).\n" +
                        "Worst of all, bad programmers are unwilling to listen or learn from mistakes because they don’t acknowledge that they’ve made mistakes — as mentioned before, they usually play the blaming game instead.\n" +
                        "Do note that this doesn’t mean cowboy coders are difficult people or jackasses in real life — they could be the nicest person you’ve met — but this arrogance and unwillingness to take responsibility for mistakes is usually ingrained in the mental attitude they take whenever faced with criticism.\n" +
                        "Any decent developer who has had the misfortune of working with a bad programmer likely has a load of horror stories to tell about these types of programmers.\n" +
                        "The Mediocre Dev\n" +
                        "\n" +
                        "No, I’m not talking about the “mediocre developer” in terms of what was referenced in Jacob Kaplan-Moss’s speech at PyCon 2015 on “the Programming Talent Myth“.\n" +
                        "The mediocre we’re referring to here is the “barely adequate” mediocre.\n" +
                        "In some ways, the mediocre dev is worse than the cowboy coder because they know they’re not great, but they are usually content with staying at the bottom of the ladder in terms of skill.\n" +
                        "Unlike cowboys, mediocre devs usually lack in an interest in programming altogether and thus have difficulties with understanding programming concepts. They take a long time to build something, but the code they produce is still subpar and filled with problems. They usually have no passion/interest in coding at all, and they are slow to learn new technologies or they’re practically untrainable.\n" +
                        "Maybe mediocre devs aren’t as destructive as cowboys because they will play in a team, but they’re definitely not bringing anything to the table and their solutions will always be worse than good developers (they’d usually create a lot of buggy/inefficient code as well due to many bad decisions).\n" +
                        "There’s not much more to say about mediocre devs. At worse they might also be pasta chefs who drag down the entire team, and at best they’re just barely making it to the finish line.\n" +
                        "The Core of the Problem\n" +
                        "At the core of what makes a developer bad is the lack of the desire to become a better programmer. Bad programmers are satisfied and comfortable with the way things currently are. Worse, both cowboys and mediocre coders usually think they know what they actually don’t know.\n" +
                        "\n" +
                        "you’ve all likely seen this meme. While this happens a lot, there’s a problem if you don’t then proceed to figure out “why”\n" +
                        "What’s more, a bad programmer is someone who is not interested in learning what they don’t know, and thus not interested in improving themselves.\n" +
                        "This is also why you’d usually find copious amounts of copy & pasting in a bad programmer’s code, as they make zero effort in figuring out why something works or doesn’t work — they just want the fix. Copy & pasting isn’t inherently bad, but only under the following circumstances:\n" +
                        "You know what you’re doing (though many bad developers would think they know what they’re doing)\n" +
                        "You’re sure that the code you’re copying & pasting will work\n" +
                        "It’s only for testing/trialing\n" +
                        "Bad developers would usually copy & paste StackOverflow code without understanding it or tweaking the solutions to fit their own code.\n" +
                        "This lack of curiosity of how a code works will cause bad developers to have a superficial understanding of the language/tools/libraries they use. Incorporating libraries/packages/what-have-you without reading the source code is also kind of similar to copying and pasting. As Jeff Atwood, the co-founder of StackOverflow says, “Read the Source Code, Luke”. How can you understand how your code works if you don’t even understand the tools you use? Of course, if you’re a beginner to programming, it may seem daunting at first to absorb all the high-level information about why things work this well, but simply accepting things as they are and knowing how to use syntaxes is the wrong attitude to take when learning how to code.\n" +
                        "On that note, people who always insist on following “best practices” without understanding why those practices are considered “best” can also be categorized as bad programmers.\n" +
                        "All in all, these guys are also called “cargo cult programmers ”, or “Programmers who understand what the code does, but not how it does it.” Perhaps you don’t need to know every detail of how a large, complex framework works. However, you should at least figure out how the part you’re using works.\n" +
                        "In addition, bad programmers don’t ever seem to learn from their mistakes, either because they don’t acknowledge they’ve made a mistake or because they have a lack of desire to learn, or the combination of the two.\n" +
                        "It’s ok to make mistakes and create bugs, because everyone makes mistakes. However, if you continue to repeat your mistakes, this means you’re not learning and that makes you a bad developer.\n" +
                        "Good Developers\n" +
                        "After rambling about bad developers for long enough, you probably already have an idea of what makes a good developer. Good developers should make up the bulk of the development workforce, and they usually have the following characteristics:\n" +
                        "Awareness that there is always going to be a better developer\n" +
                        "Humbleness and a willingness to take responsibility for mistakes as well as to learn from mistakes\n" +
                        "Writes readable, structured code\n" +
                        "Solid code design that can be debugged easily\n" +
                        "Strives to understand how things work\n" +
                        "Communicates/cooperates well with others in a team\n" +
                        "Open to criticism and different approaches\n" +
                        "Able to keep up with learning new technologies\n" +
                        "Likes solving problems\n" +
                        "Ok… quality code is very hard to measure (which is why it couldn’t be included in the quiz, but this is an important aspect of what makes a developer “good”)."
            )
        }
    }
}